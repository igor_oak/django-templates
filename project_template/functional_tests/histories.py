from unittest.mock import patch
from urllib.parse import urlparse

from django.contrib.auth import get_user_model
from django.test import LiveServerTestCase
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from registration.models import RegistrationProfile
from rest_framework.reverse import reverse
from selenium.webdriver import Firefox

class FunctionalTests(LiveServerTestCase):
    """
    Basic Live Server.
    """

    def setUp(self):
        self.driver = Firefox()
        self.driver.implicitly_wait(5)

    def tearDown(self):
        self.driver.close()

    def login(self, username, password):
        self.driver.find_element_by_id('id_username').send_keys(username)
        self.driver.find_element_by_id('id_password').send_keys(password)
        self.driver.find_element_by_id('id_login_button').click()

    def logout(self):
        self.driver.find_element_by_id('id_user_dropdown').click()
        self.driver.find_element_by_id('id_logout_button').click()
        self.driver.switch_to.alert.accept()

class UserManagementTests(FunctionalTests):
    """
    Authentication tests.
    """

    @patch('django.core.mail.EmailMultiAlternatives.send')
    def test_register(self, mock_send):
        self.assertFalse(get_user_model().objects.count())

        self.driver.get(self.live_server_url)
        self.assertIn(reverse('auth_login'), self.driver.current_url)

        self.driver.find_element_by_id('id_register_link').click()
        self.assertIn(reverse('registration_register'), self.driver.current_url)

        self.driver.find_element_by_id('id_username').send_keys('user11')
        self.driver.find_element_by_id('id_email').send_keys('email@domain.com')
        self.driver.find_element_by_id('id_password1').send_keys('1234')
        self.driver.find_element_by_id('id_password2').send_keys('1234')
        self.driver.find_element_by_id('id_register_button').click()
        mock_send.assert_called_once_with()

        self.assertTrue(get_user_model().objects.count())

        self.driver.get(self.live_server_url)
        self.assertIn(reverse('auth_login'), self.driver.current_url)

        self.login('user11', '1234')

        self.assertIn(reverse('auth_login'), self.driver.current_url)
        self.assertIn(
            'Esta conta está inativa.',
            self.driver.find_element_by_class_name('alert').text
        )

        registration_profile = RegistrationProfile.objects.get(user=get_user_model().objects.get())
        self.driver.get('{}/usuario/activate/{}/'.format(self.live_server_url, registration_profile.activation_key))
        self.assertEqual(self.driver.find_element_by_class_name('panel-heading').text, 'Ativação de conta.')

        self.driver.get(self.live_server_url)
        self.assertIn(reverse('auth_login'), self.driver.current_url)

        self.login('user11', '1234')
        self.assertEqual(reverse('home'), urlparse(self.driver.current_url).path)

        #self.logout()

    @patch('django.contrib.auth.tokens.default_token_generator.make_token')
    @patch('django.contrib.auth.tokens.default_token_generator.check_token')
    def test_password_reset(self, mock_check_token, mock_make_token):
        mock_make_token.return_value = 'token'
        mock_check_token.return_value = True

        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id('id_password_reset_link').click()
        self.assertIn(reverse('auth_password_reset'), self.driver.current_url)

        self.driver.find_element_by_id('id_email').send_keys('email@domain.com')
        self.driver.find_element_by_id('id_reset_password_button').click()

        self.assertIn(reverse('auth_password_reset_done'), self.driver.current_url)

        user = get_user_model().objects.create_user('username', 'email@domain.com', 'pass')
        password_reset_url = reverse(
            'auth_password_reset_confirm',
            args=(urlsafe_base64_encode(force_bytes(user.pk)), 'token')
        )
        self.driver.get('{}{}'.format(self.live_server_url, password_reset_url))

        self.driver.find_element_by_id('id_new_password1').send_keys('new_pass')
        self.driver.find_element_by_id('id_new_password2').send_keys('new_pass')
        self.driver.find_element_by_id('id_reset_password_button').click()

        self.assertIn(reverse('auth_password_reset_complete'), self.driver.current_url)

        self.driver.get('{}{}'.format(self.live_server_url, reverse('auth_login')))
        self.login('username', 'pass')
        self.assertIn(reverse('auth_login'), self.driver.current_url)

        self.driver.find_element_by_id('id_password').send_keys('new_pass')
        self.driver.find_element_by_id('id_login_button').click()
        self.assertEqual(reverse('home'), urlparse(self.driver.current_url).path)

        #self.logout()
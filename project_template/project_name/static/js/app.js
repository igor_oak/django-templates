/*
 * Configurações globais.
 */

var app = angular.module('{{ project_name }}', [
  '{{ project_name }}.directives', '{{ project_name }}.controllers', 'ui.bootstrap', 'ngResource', 'ngAnimate'
]);
  
app.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('[[');
  $interpolateProvider.endSymbol(']]');
});

app.config(function($resourceProvider) {
  $resourceProvider.defaults.stripTrailingSlashes = false;
});

app.config(function($httpProvider) {
  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});
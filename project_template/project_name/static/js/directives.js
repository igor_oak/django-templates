/*
 * Diretivas globais.
 */

var app = angular.module('{{ project_name }}.directives', []);

/**
 * Essa diretiva mostra/esconde o elemento em que é aplicada sempre que uma solicitação ajax é feita.
 */
app.directive('loading', function ($http) {
  return {
    restrict: 'A',
    link: function (scope, elm, attrs) {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };

      scope.$watch(scope.isLoading, function (v) {
        if (v) {
          elm.show();
        } else {
          elm.hide();
        }
      });
    }
  };
});

/**
 * Essa diretiva ativa/desativa o elemento em que é aplicada sempre que uma solicitação ajax é feita.
 */
app.directive('autoDisable', function ($http) {
  return {
    restrict: 'A',
    link: function (scope, elm, attrs) {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };

      scope.$watch(scope.isLoading, function (v) {
        if (v) {
          elm.attr('disabled', 'disabled');
        } else {
          elm.removeAttr('disabled');
        }
      });
    }
  };
});

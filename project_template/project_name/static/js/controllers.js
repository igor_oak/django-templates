/*
 * Controladores globais ou não relacionados com uma aplicação específica.
 */

var app = angular.module('{{ project_name }}.controllers', []);
app.controller('userController', function($scope) {
  $scope.alerts = [{'type': 'warning', 'closeable': true}];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  }
});

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib.admin import site

from .views import HomeView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),

    # html email.
    url(
        r'^usuario/password/reset/$',
        'django.contrib.auth.views.password_reset',
        {'html_email_template_name': 'registration/password_reset_email_html.html'},
        'password_reset'
    ),
    url(r'^usuario/password/reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),

    url(r'^usuario/', include('registration.backends.default.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(site.urls))
]

# media files in development
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
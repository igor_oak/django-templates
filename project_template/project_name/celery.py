from os import environ

from celery import Celery
from django.conf import settings

app = Celery('{{ project_name }}')
app.config_from_object(environ['DJANGO_SETTINGS_MODULE'])
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
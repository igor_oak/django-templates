from braces.views import LoginRequiredMixin
from django.views.generic import TemplateView

class HomeView(LoginRequiredMixin, TemplateView):
    """
    Home view.
    """

    template_name = 'base.html'
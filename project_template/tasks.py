from invoke import *

@task
def makemigrations(settings='development'):
    """
    Generates migration files.
    """

    cmd = './manage.py makemigrations --settings={{ project_name }}.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)

@task
def migrate(settings='development'):
    """
    Applies migrations.
    """

    cmd = './manage.py migrate --settings={{ project_name }}.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)

@task
def test(settings='development'):
    """
    Test all applications and runs coverage report.
    """

    cmd = 'coverage run --source="." --omit="tasks.py,*/wsgi.py,functional_tests/*" ./manage.py test --settings={{ project_name }}.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)
    cmd = 'coverage report'
    run(cmd, echo=True, pty=True)

@task
def functional_tests(package='functional_tests.histories', settings='development'):
    """
    Runs functional test package.
    """

    collectstatic(settings, True)
    cmd = 'coverage run --source="." ./manage.py test {} --settings={{ project_name }}.settings.{}'.format(package, settings)
    run(cmd, echo=True, pty=True)
    cmd = 'coverage report'
    run(cmd, echo=True, pty=True)

@task(default=True)
def runserver(settings='development'):
    """
    Runs local development server.
    """

    cmd = './manage.py runserver --settings={{ project_name }}.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)

@task
def collectstatic(settings='development', noinput=False):
    """
    Collects static files.
    """

    noinput = '--noinput' if noinput else ''
    cmd = './manage.py collectstatic {} --settings={{ project_name }}.settings.{}'.format(noinput, settings)
    run(cmd, echo=True, pty=True)

@task
def install(settings='development'):
    """
    Install migrations and static files.
    """

    makemigrations(settings)
    migrate(settings)
    collectstatic(settings, True)

@task
def shell(settings='development'):
    """
    Runs development shell.
    """

    cmd = './manage.py shell --settings={{ project_name }}.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)

@task
def celery(settings='development'):
    """
    Runs celery.
    """

    cmd = 'DJANGO_SETTINGS_MODULE={{ project_name }}.settings.{} celery -A {{ project_name }} worker -E -l info'.format(settings)
    run(cmd, echo=True, pty=True)

@task
def send_queued_mail(settings='development'):
    """
    Send queued django post mails.
    """

    cmd = './manage.py send_queued_mail --settings={{ project_name }}.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)


@task
def reset_postgres(database_name, version='9.4', postgres_user='postgres'):
    """
    Reset development postgres database.
    """

    cmd = '/opt/PostgreSQL/{}/bin/dropdb {} -U {}'.format(version, database_name, postgres_user)
    run(cmd, echo=True, pty=True)
    cmd = '/opt/PostgreSQL/{}/bin/createdb {} -U {}'.format(version, database_name, postgres_user)
    run(cmd, echo=True, pty=True)

@task
def reinstall_postgres(database_name, version='9.4', postgres_user='postgres'):
    """
    Drops db and reinstall migrations. DEVELOPMENT_ONLY.
    """

    reset_postgres(database_name, version, postgres_user)
    install('development')
    create_super_user('development')

@task
def create_super_user(settings='development'):
    """
    Creates django super user.
    """

    cmd = './manage.py createsuperuser --settings=representation.settings.{}'.format(settings)
    run(cmd, echo=True, pty=True)
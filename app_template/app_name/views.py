from braces.views import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic import View

class IndexView(View):
    """
    Simple index view.
    """

    def get(self, *args, **kwargs):
        return HttpResponse('Project Started')

class ProtectedView(LoginRequiredMixin, View):
    """
    Login required view.
    """

    def get(self, *args, **kwargs):
        return HttpResponse('Login configured')
from django.conf.urls import url
from .views import ProtectedView, IndexView

urlpatterns = [
   url(r'^$', IndexView.as_view()),
   url(r'^protected/$', ProtectedView.as_view())
]
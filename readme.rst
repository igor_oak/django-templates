Modelo de projeto e aplicação para Django Framework
===================================================

Introdução
----------

Modelos de projeto e aplicação Django_ customizados para o Heroku_ utilizando `Python 3.4`_.

.. _Python 3.4: http://www.python.org
.. _Django: https://www.djangoproject.com/
.. _Heroku: https://www.heroku.com/
.. _Postgres: http://www.postgresql.org/
.. _EnterpriseDB: http://www.enterprisedb.com/products-services-training/pgdownload

Dependências
------------

Além de outras dependências específicas de cada projeto, esses modelos de projeto/aplicação precisam dos seguintes pacotes *Python*::

    pip3.4 install django virtualenv

Utilização
----------
.. _documentação oficial: https://docs.djangoproject.com/en/dev/ref/django-admin/#django-admin-startapp
.. _versão do template: https://bitbucket.org/igor_oak/django-templates/downloads
.. _12factor: http://12factor.net/config

Para utilizar o modelo de projeto use o comando abaixo (*<project_name>* deve ser substituido pelo nome real do projeto e x.x.x pela `versão do template`_). Mais detalhes sobre *layout* customizado na `documentação oficial`_ ::

    django-admin.py startproject <project_name> --template=https://bitbucket.org/igor_oak/django-templates/downloads/project_template-x.x.x.zip --extension=py,rst,html,txt --name=.env,Procfile,app.js,controllers.js,directives.js

Esse modelo de projeto utiliza configurações definidas em variáveis de ambiente, seguindo recomendação da 12factor_. As variáveis de ambiente requeridas são as seguintes (assim como exemplos das mesmas)::

    export SECRET_KEY='fi_*^09lgug47ut@nge26x3ugi1&lfa#i!1(*k9e3b4fk2#-_l'
    export EMAIL_URL=smtp://:@localhost:1025 # protocol://user@domain:password@server:port
    export DATABASE_URL=postgres://postgres:admin@localhost:5432/project # engine://username:password@server:port/database
    export ADMINS=username1,email1@domain.com:username2,email2@domain.com

Vá para o diretório do projeto::

    cd <project_name>

Atribua permissão de execução para o arquivo ``<project_name>/manage.py``::

    chmod +x manage.py

Em seguida crie um ambiente virtual para o projeto e ative-o::

    python3.4 -m virtualenv ~/.venv/<project_name>
    source ~/.venv/<project_name>/bin/activate

Instale pacotes necessários para o projeto::

    sudo apt-get install libxml2-dev libxslt-dev libcairo2 libpango1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info postgresql-server-dev-9.4

Instale as dependências do projeto::

    pip3.4 install -r requirements/development.txt

Para utilizar o modelo de aplicação use o comando abaixo (*<app_name>* deve ser substituido com o nome real da aplicação e x.x.x pela `versão do template`_)::

    ./manage.py startapp <app_name> . --template=https://bitbucket.org/igor_oak/django-templates/downloads/app_template-x.x.x.zip --extension=py,rst

Inicialize uma base de dados com o mesmo nome do projeto (*<project_name>* deve ser substituido pelo nome real do projeto)::

    createdb <project_name> -U postgres

Se o comando anterior não funcionar, você precisa especificar *createdb* manualmente. Se a sua versão do Postgres_ foi instalada via EnterpriseDB_, utilize o comando a seguir (x.x é a versão do Postgres_ instalada, como por exemplo, 9.4)::

    /opt/PostgreSQL/x.x/bin/createdb <project_name> -U postgres


Inicialize os arquivos de migração, migre-os e colete arquivos estáticos::

    invoke install

Teste a aplicação recém-criada::

    invoke test

Execute a aplicação::

    invoke

Se preferir, execute tudo com apenas uma linha de comando::

    invoke install test runserver

